var fs = require('fs');
var path = require("path");

// Nombre directorio COMPONENTE
var dirComponent = "name_folder";

// Carpeta dist
var dist = "/dist/";

// Ruta node_modules ORIGEN
var nmOrigen = "/path/origen/node_modules/";
// Rute node_modules DESTINO
var nmDestino = "/path/destino/node_modules/";

// PATH ORIGEN
var pathOrigen = nmOrigen + dirComponent + dist;
// PATH DESTINO
var pathDestino = nmDestino + dirComponent + dist;


/**
 * Borrar directorio y su contenido
 * @param path 
 */
var deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

/**
 * Copia directorios
 * @param src 
 * @param dest 
 */
var copyRecursiveSync = function (src, dest) {
    var exists = fs.existsSync(src);
    var stats = exists && fs.statSync(src);
    var isDirectory = exists && stats.isDirectory();
    if (exists && isDirectory) {
        fs.mkdirSync(dest);
        fs.readdirSync(src).forEach(function (childItemName) {
            copyRecursiveSync(path.join(src, childItemName),
                path.join(dest, childItemName));
        });
    } else {
        fs.linkSync(src, dest);
    }
};



try {
    // Ejecutor funcion Borrar
    deleteFolderRecursive(pathDestino);

    // Ejecutor funcion copiar
    copyRecursiveSync(pathOrigen, pathDestino);
} catch (e) {
    switch (e.code) {
        case 'ENOENT':
            console.log("Directorio no existe!!!");
            break;

        default:
            console.log(e);
            break;
    }
}

